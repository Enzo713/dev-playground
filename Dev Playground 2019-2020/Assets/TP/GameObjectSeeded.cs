﻿using UnityEngine;

namespace TP
{
    public class GameObjectSeeded : MonoBehaviour
    {
        public Manager manager;
        public Rigidbody rb;

        private void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision other)
        {
            if(other.transform.CompareTag("Obj") == false) manager.ToPool(this.gameObject);
        }
    }
}
﻿using System.Collections.Generic;
using UnityEngine;

namespace TP
{
    public class Manager : MonoBehaviour
    {
        public List<Transform> spawnPoint;
        public List<Primitive> ObjectsToGenerate;
        public int PoolQuantity;
        public List<GameObject> pooledObjects;
    
        public float Rate;
        private float timer;
  
        void Start()
        {
          GeneratePooledObjects();
        }

        void UnPool()
        {
            int RandomObject = Random.Range(0, pooledObjects.Count);
            if (pooledObjects[RandomObject].activeSelf == false)
            {
                pooledObjects[RandomObject].transform.position = pooledObjects[RandomObject].transform.parent.position;
                pooledObjects[RandomObject].GetComponent<GameObjectSeeded>().rb.velocity = Vector3.zero;
                pooledObjects[RandomObject].transform.rotation = Quaternion.identity;
                pooledObjects[RandomObject].SetActive(true);
            }
       
       
        }

        public void ToPool(GameObject go)
        {
            go.SetActive(false);
        }
        void GeneratePooledObjects()
        {
            for (int i = 0; i < PoolQuantity; i++)
            {
                int RandomObject = Random.Range(0, ObjectsToGenerate.Count);
                ObjectsToGenerate[RandomObject].CreateObject(this);
            }
        }
        // Update is called once per frame
        void Update()
        {
            timer += Time.deltaTime;
            if (timer >= Rate)
            {
                UnPool();
                timer = 0;
            }
        }
    }
}

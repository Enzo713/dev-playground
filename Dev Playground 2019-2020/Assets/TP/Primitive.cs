﻿using System.Collections.Generic;
using UnityEngine;

namespace TP
{
    public class Primitive : ScriptableObject {
   
        public Color colorToApply;
         [HideInInspector] public int RandomSpawnPoint;
        public float MinSize;
        public float MaxSize;
        
        public virtual void CreateObject(Manager manager)
        {
        
        }

        
    }
}

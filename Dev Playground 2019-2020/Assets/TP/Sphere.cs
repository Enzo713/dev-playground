﻿using UnityEngine;

namespace TP
{
    [CreateAssetMenu (fileName = "Sphere")]
    public class Sphere : Primitive
    {
       public override void CreateObject(Manager manager)
        {
            GameObject instantiatedObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            instantiatedObj.tag = "Obj";
            var GoSeed =  instantiatedObj.AddComponent<GameObjectSeeded>();
            GoSeed.rb =  instantiatedObj.AddComponent<Rigidbody>();
            float RandomSize = Random.Range(MinSize, MaxSize);
            RandomSpawnPoint = Random.Range(0, 6);
            
            instantiatedObj.transform.parent = manager.spawnPoint[RandomSpawnPoint];
            instantiatedObj.SetActive(false);
            instantiatedObj.transform.localScale = new Vector3(RandomSize, RandomSize, RandomSize);
            instantiatedObj.GetComponent<Renderer>().material.color = colorToApply;
            GoSeed.manager = manager;
            manager.pooledObjects.Add(instantiatedObj);
        }
    }
}
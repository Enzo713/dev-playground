﻿using System;
using System.Collections;
using System.Collections.Generic;
using Pattern_Command;
using UnityEngine;

public class CommandManager : MonoBehaviour
{
    
    public List<Command> _Commands;

    private void Start()
    {
        
    }

    public void Execute()
    {
        
    }

    public void UndoAction()
    {
        
    }

    public void AddCommand()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

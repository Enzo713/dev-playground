﻿using UnityEngine;

[CreateAssetMenu(fileName = "Day")]
public class Days : ScriptableObject
{
    //Use S.O Name 
    public int HourBeginning;
    public int HourEnding;
     
        
}
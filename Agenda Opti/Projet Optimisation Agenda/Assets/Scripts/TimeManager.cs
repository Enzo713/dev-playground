﻿using System.Collections.Generic;
using UnityEngine;



    public class TimeManager : MonoBehaviour
    {
        public List<TypeOfEvent> PlaceType = new List<TypeOfEvent>();
        public List<TypeOfEvent> SplittedHours = new List<TypeOfEvent>();
        public Dictionary<string, TypeOfEvent> TypesEvent = new Dictionary<string, TypeOfEvent>();
        public int HoursPerDay;
        public int DaysPerWeek;
        public int BoardNumber; // number of superposed instances of the types,
        public AnimationCurve WeekOptimizationCurve; // From Monday To Friday, wich moment you want to condensate the most of hours

        public int DictionnaryCount;
        
        // Start is called before the first frame update
        void Start()
        {
         foreach (var types in PlaceType)
            {
                TypesEvent.Add(types.name, types);
                PreComputeHours(types);
            }

         DictionnaryCount = TypesEvent.Count;
        }

        // Update is called once per frame
        void Update()
        { 
        
        }

        public void PlaceOnCalendar(TypeOfEvent eventType)
        {
            
        }
        
        public void PreComputeHours(TypeOfEvent typeOfEvent)
        {
            if (typeOfEvent.Hours <= 8)
            {
                Debug.Log("Hours are under 8, no split needed");
                TypeOfEvent newTypeOfEvent  = ScriptableObject.CreateInstance<TypeOfEvent>();
                newTypeOfEvent.name = typeOfEvent.Name;
                newTypeOfEvent.Hours = typeOfEvent.Hours;
                newTypeOfEvent.Disponibilities = typeOfEvent.Disponibilities;
                SplittedHours.Add(newTypeOfEvent);
            }
            if (typeOfEvent.Hours > 8 && typeOfEvent.Hours <=16)
            {
                SplittedHours.Add(typeOfEvent); 
               TypeOfEvent splittedTypeOfEvent  = ScriptableObject.CreateInstance<TypeOfEvent>();
               TypeOfEvent newTypeOfEvent  = ScriptableObject.CreateInstance<TypeOfEvent>();
               newTypeOfEvent.name = typeOfEvent.Name;
               newTypeOfEvent.Hours = typeOfEvent.Hours;
               splittedTypeOfEvent.Hours = typeOfEvent.Hours - 8;
               splittedTypeOfEvent.name = typeOfEvent.Name;
               splittedTypeOfEvent.Disponibilities = typeOfEvent.Disponibilities;
               newTypeOfEvent.Hours = 8;
               TypesEvent.Add(splittedTypeOfEvent.name + 1.ToString(), splittedTypeOfEvent);
               SplittedHours.Add(splittedTypeOfEvent);
            }
            else
            {
                Debug.Log("Hours of this Type superior that 16 hours");
            }
            
        }

        
        
        public void SuperpositionChecker(TypeOfEvent eventType)
        {
            foreach (var typeEvt in TypesEvent) // Check if two same Types aren't superposed on the same time'
            {
                if (typeEvt.Value.name == eventType.Name && typeEvt.Value.IsUsed == true && typeEvt.Value.DayPlaced != eventType.DayPlaced)
                {
                    
                    Debug.Log("Type Can Be Placed Here");
                }
            }
        }
        
        public void PlaceOneType(TypeOfEvent eventType, Days day, int hourBegin, int hourStop)
        {
            day =  ScriptableObject.CreateInstance<Days>();
            day.HourBeginning = hourBegin;
            day.HourEnding = hourStop;

            if (eventType.CheckHourDone(hourBegin, hourStop) > eventType.Hours)
            {
                eventType.IsUsed = false;
                eventType.Hours = eventType.CheckHourDone(hourBegin, hourStop);
            }
           else
            {
                eventType.IsUsed = true;
            }
        }
        
        
    }

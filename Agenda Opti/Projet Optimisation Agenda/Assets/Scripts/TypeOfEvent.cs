﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Type")]
public class TypeOfEvent : ScriptableObject
{
    public string Name;
    public int Hours;
    public int Promotion;
    
    public bool IsUsed;
    public List<Days> DayPlaced;
        
    public int AgendaIndex;
    public List<Days> Disponibilities;

    public int CheckHourDone(int begin, int end)
    {
        return end - begin;
    }
        
    public void DisponibilityChecker(TimeManager manager)
    {
        foreach (var day in Disponibilities)
        {
            foreach (var dayPlaced in DayPlaced)
            {
                if (day == dayPlaced)
                {
                    Debug.Log("Day Is Okay");
                    break;
                }
                    
            }
                
        }
    }
        
}